
#include "BioEnemyShip.h"

//Emmett created a description of a method on this page.

BioEnemyShip::BioEnemyShip() //Creates a Bio Enemy Ship.
{
	SetSpeed(150); //Sets the ship's speed.
	SetMaxHitPoints(1); //Sets the ship's hit points.
	SetCollisionRadius(20); //Sets the collision radius.
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
