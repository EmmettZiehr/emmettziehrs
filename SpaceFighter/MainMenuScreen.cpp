
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Emmett Ziehr created a description of a method on this page.

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr; 

	SetRemoveCallback(OnScreenRemove); 

	SetTransitionInTime(2.0f); //Sets the time it takes for the main menu screen to fade in. Changed by Emmett from 1.0f to 2.0f. 
	SetTransitionOutTime(0.5f); //Sets the time it takes for the main menu screen to fade out.

	Show(); // Shows the screen.
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png"); //Loads the logo into the game.
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150; //Positions the logo in the center of the main menu screen.

	// Create the menu items
	const int COUNT = 2; //Decalares a variable called "Count".
	MenuItem *pItem; //Creates a new menu Item on the heap.
	Font::SetLoadSize(20, true); //Loads the font size of the menu items.
	Font *pFont = pResourceManager->Load<Font>("Fonts\\ariblk.ttf"); // Sets the font. The font was changed from Ethnocentric to Ariblk by Emmett.

	SetDisplayCount(COUNT); //Sets the amount of displayed items to 2.

	enum Items { START_GAME, QUIT}; //An enumerator that sets the order of what the main menu options do.
	std::string text[COUNT] = { "Start Game <3", "Quit" }; //Delcares what the main menus options say.

	for (int i = 0; i < COUNT; i++) // A for-loop used to generate the menu items to prevent the duplication of code.
	{
		pItem = new MenuItem(text[i]); //Creates a new menu item on the stack.
		pItem->SetPosition(Vector2(100, 100 + 50 * i)); //Sets the location of the newly created menu item.
		pItem->SetFont(pFont); //Sets the font of the menu items.
		pItem->SetColor(Color::Blue); //Sets the default color of the menu items.
		pItem->SetSelected(i == 0); //Decides whether or not the menu item is currently selected.
		AddMenuItem(pItem); //Adds the menu item.
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect); //Calls back the "Start Game" menu item when selected.
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect); //Calls back the "Quit" menu item when selected.
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Red); //Changed from white to red by Emmett.
		else pItem->SetColor(Color::Purple); //Changed fom blue to purple by Emmett.
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
