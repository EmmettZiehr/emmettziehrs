
#include "Ship.h"

//Emmett created a description of a method on this page.

Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage) // A method that deduces the life points of a ship after being hit & deactivates it when the hit points reach zero.
{
	if (!m_isInvulnurable) // A conditional statement that runs if the ship isn't invulnerable.
	{
		m_hitPoints -= damage; // Deduces the life points of a ship.

		if (m_hitPoints <= 0) // A condition statement activatedd when the hit points member variable reaches zero.
		{
			GameObject::Deactivate(); //Deactivates the object.
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}